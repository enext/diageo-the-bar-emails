# DIAGEO THE BAR EMAILS

Este repositório é destinado ao desenvolvimento de e-mail marketing para o cliente **Diageo / The Bar**.

1. Crie um arquivo `.json` no diretório `app/data` com o id do e-mail que você quer editar.

2. Agora crie um arquivo `.hbs` no diretório `app/templates` com o mesmo id do arquivo `json`

3. Inicie o **Live Reload** com a extensão **Live Server** do VS Code

**ATENÇÃO:** O diretório `build/` é somente para a visualização do _HTML_, não coloque ele no editor da VTEX.
