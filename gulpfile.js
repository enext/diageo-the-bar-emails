const { src, dest, parallel, series, watch } = require("gulp");
const through = require("through2");
const path = require("path");

var browserSync = require("browser-sync").create();
var reload = browserSync.reload;

// Gulp libraries
const hb = require("gulp-hb");
const webserver = require("gulp-webserver");
const ext_replace = require("gulp-ext-replace");

const build = () =>
    src(["./app/data/*.json"]).pipe(
        through.obj((file, enc, cb) => {
            const filename = path.basename(file.path).replace(".json", ".hbs");

            src(`app/templates/${filename}`)
                .pipe(
                    hb()
                        .data(JSON.parse(file.contents.toString()))
                        .partials("./app/partials/**/*.hbs")
                        .helpers(
                            require("just-handlebars-helpers/lib/helpers/conditionals.js")
                        )
                        .helpers("./app/helpers/*.helper.js")
                )
                .pipe(ext_replace(".html"))
                .pipe(dest("./build/"))
                .on("error", cb)
                .on("end", cb);
        })
    );

const serve = () => {
    //   browserSync.init({
    //     debug: true,
    //     server: {
    //       baseDir: "./build",
    //       serveStaticOptions: {
    //         extensions: ["html"]
    //       }
    //     },
    //     open: false,
    //     reloadOnRestart: true,
    //   });

    watch(["app/**/*.hbs", "app/**/*.json"], parallel(build));
};

exports.default = series(build, serve);
